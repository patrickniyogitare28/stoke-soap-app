//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2021.06.25 at 04:01:46 PM CAT 
//


package https.org_ac_rca_stock.items;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the https.org_ac_rca_stock.items package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: https.org_ac_rca_stock.items
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DeleteItemResponse }
     * 
     */
    public DeleteItemResponse createDeleteItemResponse() {
        return new DeleteItemResponse();
    }

    /**
     * Create an instance of {@link Item }
     * 
     */
    public Item createItem() {
        return new Item();
    }

    /**
     * Create an instance of {@link NewItemDtoRespnose }
     * 
     */
    public NewItemDtoRespnose createNewItemDtoRespnose() {
        return new NewItemDtoRespnose();
    }

    /**
     * Create an instance of {@link GetAllItemsDtoRequest }
     * 
     */
    public GetAllItemsDtoRequest createGetAllItemsDtoRequest() {
        return new GetAllItemsDtoRequest();
    }

    /**
     * Create an instance of {@link UpdateItemRequest }
     * 
     */
    public UpdateItemRequest createUpdateItemRequest() {
        return new UpdateItemRequest();
    }

    /**
     * Create an instance of {@link NewItemDtoRequest }
     * 
     */
    public NewItemDtoRequest createNewItemDtoRequest() {
        return new NewItemDtoRequest();
    }

    /**
     * Create an instance of {@link UpdateItemResponse }
     * 
     */
    public UpdateItemResponse createUpdateItemResponse() {
        return new UpdateItemResponse();
    }

    /**
     * Create an instance of {@link GetItemDetailsDtoRequest }
     * 
     */
    public GetItemDetailsDtoRequest createGetItemDetailsDtoRequest() {
        return new GetItemDetailsDtoRequest();
    }

    /**
     * Create an instance of {@link GetItemDetailsResponse }
     * 
     */
    public GetItemDetailsResponse createGetItemDetailsResponse() {
        return new GetItemDetailsResponse();
    }

    /**
     * Create an instance of {@link GetAllItemsResponse }
     * 
     */
    public GetAllItemsResponse createGetAllItemsResponse() {
        return new GetAllItemsResponse();
    }

    /**
     * Create an instance of {@link DeleteItemRequest }
     * 
     */
    public DeleteItemRequest createDeleteItemRequest() {
        return new DeleteItemRequest();
    }

}
