package org.ac.rca.stock.stock.repositories;

import org.ac.rca.stock.stock.models.Item;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IItemsRepository extends JpaRepository<Item, Long> {
}
