package org.ac.rca.stock.stock.endpoints;

import antlr.collections.List;
import https.org_ac_rca_stock.suppliers.*;
import org.ac.rca.stock.stock.repositories.ISuppliersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import java.util.Optional;
@Endpoint
public class SupplierEndPoints {
    private final ISuppliersRepository suppliersRepository;

    @Autowired
    public SupplierEndPoints(ISuppliersRepository suppliersRepository) {
        this.suppliersRepository = suppliersRepository;
    }


    @PayloadRoot(namespace = "https://org.ac.rca.stock/suppliers", localPart = "NewSupplierDtoRequest")
    @ResponsePayload
    public NewSupplierDtoResponse create(@RequestPayload NewSupplierDtoRequest dto) {
        Supplier __supplier = dto.getSupplier();
        org.ac.rca.stock.stock.models.Supplier supplier = new org.ac.rca.stock.stock.models.Supplier(__supplier.getNames(),__supplier.getEmail(), __supplier.getMobile());
        org.ac.rca.stock.stock.models.Supplier supplier1 = suppliersRepository.save(supplier);
        NewSupplierDtoResponse supplierDtoResponse = new NewSupplierDtoResponse();
        supplier.setId(supplier1.getId());
        supplierDtoResponse.setSupplier(supplier);
        return supplierDtoResponse;
    }

    @PayloadRoot(namespace = "https://org.ac.rca.stock/suppliers", localPart = "GetAllSuppliersRequest")
    @ResponsePayload
    public GetAllSuppliersResponse findAll(@RequestPayload GetAllSuppliersRequest request){

        List<org.ac.rca.stock.stock.models.Supplier> suppliers = suppliersRepository.findAll();

        GetAllSuppliersResponse response = new GetAllSuppliersResponse();

        for (org.ac.rca.stock.stock.models.Supplier supplier: suppliers){
            Supplier _supplier = new Supplier();
            _supplier.setId(supplier.getId());
            _supplier.setNames(supplier.getNames());
            _supplier.setEmail(supplier.getEmail());
            _supplier.setMobile(supplier.getMobile());
            response.getSupplier().add(_supplier);
        }

        return response;
    }

    @PayloadRoot(namespace = "https://org.ac.rca.stock/suppliers", localPart = "GetSupplierDetailsRequest")
    @ResponsePayload
    public GetSupplierDetailsResponse findById(@RequestPayload GetSupplierDetailsRequest request){
        Optional<org.ac.rca.stock.stock.models.Supplier> _supplier = suppliersRepository.findById(request.getId());

        if(!_supplier.isPresent())
            return new GetSupplierDetailsResponse();

        org.ac.rca.stock.stock.models.Supplier supplier = _supplier.get();

        GetSupplierDetailsResponse response = new GetSupplierDetailsResponse();

        Supplier __supplier = new Supplier();
        __supplier.setId(supplier.getId());
        __supplier.setNames(supplier.getNames());
        __supplier.setEmail(supplier.getEmail());
        __supplier.setMobile(supplier.getMobile());


        response.setSupplier(__supplier);

        return response;
    }

    @PayloadRoot(namespace = "https://org.ac.rca.stock/suppliers", localPart = "DeleteSupplierRequest")
    @ResponsePayload
    public DeleteSupplierResponse delete(@RequestPayload DeleteSupplierRequest request){
        suppliersRepository.deleteById(request.getId());
        DeleteSupplierResponse response = new DeleteSupplierResponse();
        response.setMessage("Successfully deleted a message");
        return response;
    }

    @PayloadRoot(namespace = "https://org.ac.rca.stock/suppliers", localPart = "UpdateSupplierRequest")
    @ResponsePayload
    public UpdateSupplierResponse update(@RequestPayload UpdateSupplierRequest request){
        Supplier __supplier = request.getSupplier();

        org.ac.rca.stock.stock.models.Supplier _supplier = new org.ac.rca.stock.stock.models.Supplier(__supplier.getNames(), __supplier.getEmail(), __supplier.getMobile());
        _supplier.setId(__supplier.getId());

        org.ac.rca.stock.stock.models.Supplier supplier = suppliersRepository.save(_supplier);

        UpdateSupplierResponse supplierDto = new UpdateSupplierResponse();

        __supplier.setId(supplier.getId());

        supplierDto.setSupplier(__supplier);

        return supplierDto;
    }


}
