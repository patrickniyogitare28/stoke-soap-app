package org.ac.rca.stock.stock.config;


import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;


@EnableWs //Enable Spring Web Services
@Configuration //Spring Configuration
public class WebServicesConfig {

    // MessageDispatcherServlet
    // ApplicationContext
    // url -> /ws/*

    @Bean
    public ServletRegistrationBean<MessageDispatcherServlet> messageDispatcherServlet(ApplicationContext context) {
        MessageDispatcherServlet messageDispatcherServlet = new MessageDispatcherServlet();
        messageDispatcherServlet.setApplicationContext(context);
        messageDispatcherServlet.setTransformWsdlLocations(true);
        return new ServletRegistrationBean<MessageDispatcherServlet>(messageDispatcherServlet, "/ws/patrick/*");
    }

    // /ws/patrick/suppliers.wsdl
    // course-details.xsd
    @Bean(name = "suppliers")
    public DefaultWsdl11Definition studentWsdl(XsdSchema supplierSchema) {
        DefaultWsdl11Definition definition = new DefaultWsdl11Definition();
        definition.setPortTypeName("SupplierPort");
        definition.setTargetNamespace("https://org.ac.rca.stock/suppliers");
        definition.setLocationUri("/ws/patrick/");
        definition.setSchema(supplierSchema);
        return definition;
    }

    @Bean(name = "items")
    public DefaultWsdl11Definition courseWsdl(XsdSchema itemsSchema) {
        DefaultWsdl11Definition definition = new DefaultWsdl11Definition();
        definition.setPortTypeName("ItemsPort");
        definition.setTargetNamespace("https://org.ac.rca.stock/items");
        definition.setLocationUri("/ws/patrick/");
        definition.setSchema(itemsSchema);
        return definition;
    }

    @Bean
    public XsdSchema supplierSchema() {
        return new SimpleXsdSchema(new ClassPathResource("suppliers.xsd"));
    }

    @Bean
    public  XsdSchema itemsSchema(){
        return new SimpleXsdSchema(new ClassPathResource("items.xsd"));
    }
}

