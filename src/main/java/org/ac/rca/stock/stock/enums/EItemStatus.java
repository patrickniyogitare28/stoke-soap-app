package org.ac.rca.stock.stock.enums;

public enum EItemStatus {
    NEW, GOOD_SHAPE, OLD
}
