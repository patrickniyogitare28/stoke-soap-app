package org.ac.rca.stock.stock.models;

import com.sun.istack.NotNull;
import org.ac.rca.stock.stock.enums.EItemStatus;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Item {
    @GeneratedValue
    @Id
    private long id;

    @NotNull
    private String name;

    @NotNull
    private String itemCode;


    @NotNull
    private EItemStatus status;

    @NotNull
    private int price;

    @NotNull
    private long supplier;


    public Item(String name, String itemCode, EItemStatus status, int price, long supplier) {
        this.name = name;
        this.itemCode = itemCode;
        this.status = status;
        this.price = price;
        this.supplier = supplier;
    }

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public EItemStatus getStatus() {
        return status;
    }

    public void setStatus(EItemStatus status) {
        this.status = status;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public long getSupplier() {
        return supplier;
    }

    public void setSupplier(long supplier) {
        this.supplier = supplier;
    }
}
