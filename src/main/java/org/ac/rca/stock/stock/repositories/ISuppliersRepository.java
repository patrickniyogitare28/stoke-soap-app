package org.ac.rca.stock.stock.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.ac.rca.stock.stock.models.Supplier;

public interface ISuppliersRepository extends JpaRespository<Supplier, Long>{
}
